import React, { Component } from 'react'
import App from '../App/index'
import Footer from './Footer'
import Header from './Header'


export class Layout extends Component {
  static propTypes = {}

  render() {
    return (
    <>
    <Header/>
    <App/>
    <Footer/>
    </>
    )
  }
}

export default Layout