import React, { Component } from 'react'
import styles from '../Golbal-Css/mystyle.module.css'
import './Footer.css'
import {Helmet} from "react-helmet";

export class Footer extends Component {
  render() {
    const mystyle = {
        color: "white",
        backgroundColor: "DodgerBlue",
        padding: "5px",
        fontFamily: "Arial"
      };
    return (
    <>
    
    <Helmet>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"  type="text/javascript" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"  type="text/javascript" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css"  type="text/javascript" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"  type="text/javascript" />
    <script src= "assets/css/style.css"/>
    </Helmet>
      <div>
      <p className= {styles.bigblack}>----------------------------------</p>
      <h1 Style= {mystyle}>I am a Footer</h1>
      <div class="footer-basic">
        <footer>
            <ul class="list-inline">
                <li class="list-inline-item"><a href="#">Home</a></li>
                <li class="list-inline-item"><a href="#">Services</a></li>
                <li class="list-inline-item"><a href="#">About</a></li>
                <li class="list-inline-item"><a href="#">Terms</a></li>
                <li class="list-inline-item"><a href="#">Privacy Policy</a></li>
            </ul>
            <p class="copyright">Augments System Pvt. Ltd. © 2008</p>
        </footer>
        
    </div>
       
      </div>
      <h1>this is footer page</h1>
    </>
    )
  }
}

export default Footer