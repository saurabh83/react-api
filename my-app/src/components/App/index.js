import React, { Component } from 'react'
import './App.css'

export class App extends React.Component {
       // Constructor 
       constructor(props) {
        super(props);
   
        this.state = {
            items: [],
            DataisLoaded: false
        };
    }
    // ComponentDidMount is used to
    // execute the code 
    componentDidMount() {
        fetch(
"https://jsonplaceholder.typicode.com/users")
            .then((res) => res.json())
            .then((json) => {
                this.setState({
                    items: json,
                    DataisLoaded: true
                });
            })
    }
  render() {
    const { DataisLoaded, items } = this.state;
        if (!DataisLoaded) return <div>
            <h1> Pleses wait some time.... </h1> </div> ;
   
    const mystyle = {
        color: "white",
        backgroundColor: "DodgerBlue",
        padding: "5px",
        fontFamily: "Arial"
      };
    return (
    <>
      <div className='App'>
      <h1 Style= {mystyle}>Main App</h1>
      <h1> Fetch data from an api in react </h1> 
      <h3 style={{color: "black"}}> {
                items.map((item) => ( 
                <ol key = { item.id } >
                    User_Name: { item.username }, 
                    Full_Name: { item.name }, 
                    User_Email: { item.email } 
                    </ol>
                     ))
                    }
                    </h3>
      </div>
    </>
    )
  }
}



export default App
